﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Microsoft.SyndicationFeed.Rss;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        // GET api/values
        //[HttpGet]
        // public ActionResult<IEnumerable<string>> Get()
        // {
        //     return new string[] { "value1", "value2" };
        // } 
        [HttpGet]

        public async Task<ActionResult<IEnumerable<string>>> Get()
        {
            HttpClient client = new HttpClient();
            string rss = await client.GetStringAsync("https://blogs.msdn.microsoft.com/appconsult/feed/");

            List<string> news = new List<string>();

            using (var xmlReader = XmlReader.Create(new StringReader(rss), new XmlReaderSettings { Async = true, DtdProcessing = DtdProcessing.Parse, ValidationType = ValidationType.DTD }))
            {
                //RssFeedReader feedReader = new RssFeedReader(reader);
                RssFeedReader feedReader = new RssFeedReader(xmlReader);
                while (await feedReader.Read())
                {
                    if (feedReader.ElementType == Microsoft.SyndicationFeed.SyndicationElementType.Item)
                    {
                        var item = await feedReader.ReadItem();
                        news.Add(item.Title);
                    }
                }
            }
            return news;
        }
    }
    // public async Task<ActionResult<IEnumerable<string>>> Get()
    // {
    //     HttpClient client = new HttpClient();
    //     string rss = await client.GetStringAsync("https://blogs.msdn.microsoft.com/appconsult/feed/");

    //     List<string> news = new List<string>();

    //     using (var xmlReader = XmlReader.Create(new StringReader(rss), new XmlReaderSettings { Async = true }))
    //     {
    //         RssFeedReader feedReader = new RssFeedReader(xmlReader);
    //         while (await feedReader.Read())
    //         {
    //             if (feedReader.ElementType == Microsoft.SyndicationFeed.SyndicationElementType.Item)
    //             {
    //                 var item = await feedReader.ReadItem();
    //                 news.Add(item.Title);
    //             }
    //         }
    //     }
    //     return news;
    // }

}

